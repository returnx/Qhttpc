#ifndef QHTTPC_H
#define QHTTPC_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QHttpPart>
#include <QEventLoop>
#include <QUrlQuery>
#include "qhttpc_response.h"

class qhttpc : public QObject
{
    Q_OBJECT
public:

    explicit qhttpc(QObject *parent = 0);

    enum e_media_type
    {
        json=1,
        form=2,
        multipart=3,
        html=4
    };

    void async_http_get_request(QUrl any_url);
    void async_http_post_request(QUrl any_url, QByteArray data);
    void async_http_post_request(QUrl any_url, QUrlQuery parameters, e_media_type media_type);

    qhttpc_response *http_get_request(const QUrl &any_url);
    qhttpc_response *http_post_request(const QUrl &any_url, const QByteArray &data);
    qhttpc_response *http_post_request(const QUrl &any_url, const QUrlQuery &parameters, e_media_type media_type,
                                 const QMap<QByteArray, QByteArray> &headers = QMap<QByteArray, QByteArray> ());

    qhttpc_response *http_custom_request(const QUrl &any_url, const QByteArray &custom_verb, const QUrlQuery &parameters,
                                         e_media_type media_type,
                                         const QMap<QByteArray, QByteArray> &headers = QMap<QByteArray, QByteArray> ());



    int status_code();

    bool is_error() const;

    QString error_string() const;

signals:
    void recive_data_finished(QByteArray recive_data);
        void recive_progress_changed(quint64,quint64);
        //void request_error(base_exception *ex);
        void request_error(QString error);

    public slots:
        void request_reply_finished(QNetworkReply *);
        void network_reply_progress_changed(qint64 bytes_read, qint64 recive_block_size);
        void network_reply_error(QNetworkReply::NetworkError error);
        void sync_request_reply_finished(QNetworkReply *);

    private :

        QNetworkAccessManager m_network_access_manager;
        QNetworkReply *m_network_reply;
        QEventLoop m_event_loop;
        int m_status_code;
        //QByteArray m_response;
        qhttpc_response *m_response = nullptr;
        bool m_is_async;
        bool m_is_error;
        QString m_error_string;


};

#endif

#include "qhttpc_response.h"

qhttpc_response::qhttpc_response(QObject *parent) : QObject(parent)
{
    m_status_code = -1;
    m_size = 0;
    m_body.clear();
    m_headres.clear();
}

int qhttpc_response::status_code() const
{
    return m_status_code;
}

void qhttpc_response::set_status_code(const int &status_code)
{
    m_status_code = status_code;
}

qint64 qhttpc_response::size() const
{
    return m_size;
}

void qhttpc_response::set_size(const quint64 &value)
{
    m_size = value;
}

QByteArray qhttpc_response::body() const
{
    return m_body;
}

void qhttpc_response::set_body(const QByteArray &response)
{
    m_body = response;
}

QMap<QByteArray, QByteArray> qhttpc_response::headres() const
{
    return m_headres;
}

void qhttpc_response::set_headres(const QMap<QByteArray, QByteArray> &value)
{
    m_headres = value;
}

//http_response &http_response::operator=(const http_response &r_val)
//{
//    this.set_body(r_val.body());
//    this.set_size(r_val.size());
//    this.set_status_code(r_val.status_code());
//    return *this;
//}

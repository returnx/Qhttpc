#ifndef QHTTPC_RESPONSE_H
#define QHTTPC_RESPONSE_H

#include <QObject>
#include <QMap>

class qhttpc_response : public QObject
{
    Q_OBJECT
public:

    explicit qhttpc_response(QObject *parent = nullptr);

    int status_code() const;
    void set_status_code(const int &status_code);

    qint64 size() const;
    void set_size(const quint64 &value);

    QByteArray body() const;
    void set_body(const QByteArray &body);


    QMap<QByteArray, QByteArray> headres() const;
    void set_headres(const QMap<QByteArray, QByteArray> &value);

   // http_response & operator=(const http_response &r_val);

signals:

public slots:

private:
    int m_status_code;
    qint64 m_size;
    QByteArray m_body;
    QMap<QByteArray, QByteArray> m_headres;

};

#endif // HTTP_RESPONSE_H

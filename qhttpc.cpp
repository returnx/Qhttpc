#include "qhttpc.h"
#include "qhttpc_response.h"

// TODO: Post function most be compatible by new approach.
// TODO: Asyncs function must be compatible by new approach.

qhttpc::qhttpc(QObject *parent) : QObject(parent)
{

}


void qhttpc::async_http_get_request(QUrl any_url)
{
    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(request_reply_finished(QNetworkReply*)));

    //    if (any_url.isValid()) //Check is url ise valid do:
    //    {
    QNetworkRequest i_request(any_url); //Create an Network request
    //Connect network reply object signals:
    m_network_reply=m_network_access_manager.get(i_request); //Send Get methode Request!
    connect(m_network_reply,SIGNAL(downloadProgress(qint64,qint64))
            ,SLOT(network_reply_progress_changed(qint64,qint64)));
    // connect(m_network_reply,SIGNAL(finished()),SLOT(request_reply_finished()));
    connect (m_network_reply,SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));

    //    }
    //    else
    //        throw std::runtime_error("URL is not valid.");

}

void qhttpc::async_http_post_request(QUrl any_url, QByteArray data)
{

    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(request_reply_finished(QNetworkReply*)));
    QNetworkRequest i_request(any_url);

    i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
    m_network_reply=m_network_access_manager.post(i_request,data);

    connect(m_network_reply,SIGNAL(downloadProgress(qint64,qint64))
            ,SLOT(network_reply_progress_changed(qint64,qint64)));
    // connect(m_network_reply,SIGNAL(finished()),SLOT(request_reply_finished()));
    connect (m_network_reply,SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));
}

void qhttpc::async_http_post_request(QUrl any_url,QUrlQuery parameters,e_media_type media_type)
{

    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(request_reply_finished(QNetworkReply*)));
    QNetworkRequest i_request(any_url);

    /*
        json="application/json",
        form="application/x-www-form-urlencoded",
        multipart="multipart/form-data",
        html="text/html"
        */
    switch (media_type)
    {
    case json:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
        break;
    case form:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        break;
    case multipart:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    case html:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    }

    qDebug()<<parameters.toString().toUtf8();
    m_network_reply = m_network_access_manager.post(i_request,parameters.toString().toUtf8());

    connect(m_network_reply,SIGNAL(downloadProgress(qint64,qint64))
            ,SLOT(network_reply_progress_changed(qint64,qint64)));
    // connect(m_network_reply,SIGNAL(finished()),SLOT(request_reply_finished()));
    connect (m_network_reply,SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));
}

qhttpc_response* qhttpc::http_get_request(const QUrl &any_url)
{

    // Init new response
    m_response = new qhttpc_response(this);

    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(sync_request_reply_finished(QNetworkReply*)));
    m_is_async = false;
    QNetworkRequest i_request(any_url); //Create an Network request
    //Connect network reply object signals:
    m_network_reply = m_network_access_manager.get(i_request); //Send Get methode Request!

    connect(m_network_reply, SIGNAL(downloadProgress(qint64, qint64))
            , SLOT(network_reply_progress_changed(qint64, qint64)));

    connect (m_network_reply, SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));

    m_event_loop.exec();
    return m_response;
}

qhttpc_response *qhttpc::http_post_request(const QUrl &any_url, const QByteArray &data)
{

    m_response = new qhttpc_response(this);

    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(sync_request_reply_finished(QNetworkReply*)));

    m_is_async = false;

    QNetworkRequest i_request(any_url);

    m_network_reply = m_network_access_manager.post(i_request, data);

    connect(m_network_reply, SIGNAL(downloadProgress(qint64, qint64))
            , SLOT(network_reply_progress_changed(qint64, qint64)));

    connect (m_network_reply, SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));

    m_event_loop.exec();
    return m_response;
}

qhttpc_response *qhttpc::http_post_request(const QUrl &any_url, const QUrlQuery &parameters, e_media_type media_type,
                                           const QMap<QByteArray, QByteArray> &headers)
{
    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(sync_request_reply_finished(QNetworkReply*)));
    QNetworkRequest i_request(any_url);

    /*
        json="application/json",
        form="application/x-www-form-urlencoded",
        multipart="multipart/form-data",
        html="text/html"
        */
    switch (media_type)
    {
    case json:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
        break;
    case form:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        break;
    case multipart:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    case html:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    }

    //Set custom header
    foreach (QByteArray key, headers.keys())
    {
        i_request.setRawHeader(key, headers.value(key));
    }

    m_network_reply = m_network_access_manager.post(i_request, parameters.toString().toUtf8());

    connect(m_network_reply,SIGNAL(downloadProgress(qint64,qint64))
            ,SLOT(network_reply_progress_changed(qint64,qint64)));

    connect (m_network_reply,SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));
    m_event_loop.exec();
    return m_response;
}

qhttpc_response *qhttpc::http_custom_request(const QUrl &any_url, const QByteArray &custom_verb, const QUrlQuery &parameters,
                                             e_media_type media_type,
                                             const QMap<QByteArray, QByteArray> &headers)
{
    connect(&m_network_access_manager, SIGNAL(finished(QNetworkReply*)),
            SLOT(sync_request_reply_finished(QNetworkReply*)));
    QNetworkRequest i_request(any_url);
    switch (media_type)
    {
    case json:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/json");
        break;
    case form:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");
        break;
    case multipart:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    case html:
        i_request.setHeader(QNetworkRequest::ContentTypeHeader,"multipart/form-data");
        break;
    }

    //Set custom header
    foreach (QByteArray key, headers.keys())
    {
        i_request.setRawHeader(key, headers.value(key));
    }

    qDebug()<<parameters.toString().toUtf8();
    m_network_reply = m_network_access_manager.sendCustomRequest(i_request,
                                                                 custom_verb,
                                                                 parameters.toString().toUtf8());

    connect(m_network_reply,SIGNAL(downloadProgress(qint64,qint64))
            ,SLOT(network_reply_progress_changed(qint64,qint64)));

    connect (m_network_reply,SIGNAL(error(QNetworkReply::NetworkError)),
             SLOT(network_reply_error(QNetworkReply::NetworkError)));
    m_event_loop.exec();

    return m_response;
}

void qhttpc::network_reply_progress_changed(qint64 bytes_read, qint64 recive_block_size)
{
    if (recive_block_size > -1)
        m_response->set_size((quint64)recive_block_size);
    emit recive_progress_changed(bytes_read,recive_block_size);
}


void qhttpc::network_reply_error(QNetworkReply::NetworkError error)
{
    m_is_error = true;
    m_error_string = m_network_reply->errorString();
    emit request_error(m_network_reply->errorString());
    /*
   base_exception *ex=new base_exception(this);
    switch (error)
    {
    case QNetworkReply::ConnectionRefusedError:
        ex->set_error_message("ConnectionRefusedError");
        break;
    case QNetworkReply::RemoteHostClosedError:
        ex->set_error_message("RemoteHostClosedError");
        break;
    case QNetworkReply::HostNotFoundError:
         ex->set_error_message("HostNotFoundError");
        break;
    case QNetworkReply::TimeoutError :
        ex->set_error_message("TimeoutError");
        break;
    case QNetworkReply::OperationCanceledError :
        ex->set_error_message("OperationCanceledError");
        break;
    case QNetworkReply::SslHandshakeFailedError:
        ex->set_error_message("SslHandshakeFailedError");
        break;
    case QNetworkReply::TemporaryNetworkFailureError:
        ex->set_error_message("TemporaryNetworkFailureError");
        break;
    case QNetworkReply::NetworkSessionFailedError:
        ex->set_error_message("the connection was broken due to disconnection from the network or failure to start the network.");
        break;
      case QNetworkReply::BackgroundRequestNotAllowedError:
        ex->set_error_message("the background request is not currently allowed due to platform policy.");
        break;
      case QNetworkReply::ProxyConnectionRefusedError:
        ex->set_error_message("the connection to the proxy server was refused (the proxy server is not accepting requests)");
        break;
     case QNetworkReply::ProxyConnectionClosedError:
        ex->set_error_message("the proxy server closed the connection prematurely, before the entire reply was received and processed");
        break;
     case QNetworkReply::ProxyNotFoundError:
        ex->set_error_message("the proxy host name was not found (invalid proxy hostname)");
        break;
     case QNetworkReply::ProxyTimeoutError:
        ex->set_error_message("the connection to the proxy timed out or the proxy did not reply in time to the request sent");
        break;
      case QNetworkReply::ProxyAuthenticationRequiredError:
        ex->set_error_message("the proxy requires authentication in order to honour the request but did not accept any credentials offered (if any)");
        break;
      case QNetworkReply::ContentAccessDenied:
        ex->set_error_message("the access to the remote content was denied (similar to HTTP error 401)");
        break;
       case QNetworkReply::ContentOperationNotPermittedError:
        ex->set_error_message("the operation requested on the remote content is not permitted");
        break;
     case QNetworkReply::ContentNotFoundError:
        ex->set_error_message("the remote content was not found at the server (similar to HTTP error 404)");
        break;
     case QNetworkReply::AuthenticationRequiredError:
        ex->set_error_message("the remote server requires authentication to serve the content but the credentials provided were not accepted (if any)");
        break;
      case QNetworkReply::ContentReSendError:
        ex->set_error_message("the request needed to be sent again, but this failed for example because the upload data could not be read a second time.");
        break;
    case QNetworkReply::ContentConflictError:
        ex->set_error_message("the request could not be completed due to a conflict with the current state of the resource.");
        break;
    case QNetworkReply::ContentGoneError:
        ex->set_error_message("the requested resource is no longer available at the server.");
        break;
    case QNetworkReply::InternalServerError:
        ex->set_error_message("the server encountered an unexpected condition which prevented it from fulfilling the request.");
        break;
    case QNetworkReply::OperationNotImplementedError:
        ex->set_error_message("the server does not support the functionality required to fulfill the request.");
        break;
    case QNetworkReply::ServiceUnavailableError:
        ex->set_error_message("the server is unable to handle the request at this time.");
        break;
    case QNetworkReply::ProtocolUnknownError:
        ex->set_error_message("the Network Access API cannot honor the request because the protocol is not known");
        break;
    case QNetworkReply::ProtocolInvalidOperationError:
        ex->set_error_message("the requested operation is invalid for this protocol");
        break;
    case QNetworkReply::UnknownNetworkError:
        ex->set_error_message("an unknown network-related error was detected");
        break;
    case QNetworkReply::UnknownProxyError:
        ex->set_error_message("an unknown proxy-related error was detected");
        break;
    case QNetworkReply::UnknownContentError:
        ex->set_error_message("an unknown error related to the remote content was detected");
        break;
    case QNetworkReply::ProtocolFailure:
        ex->set_error_message("a breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.)");
        break;
    case QNetworkReply::UnknownServerError:
        ex->set_error_message("an unknown error related to the server response was detectedv");
        break;
    }

    ex->set_error_code(int(error));
    emit request_error(ex);
    */

}

void qhttpc::request_reply_finished(QNetworkReply *reply)
{
    QByteArray recive_data=reply->readAll();
    m_status_code=m_network_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    m_network_reply->deleteLater();
    emit recive_data_finished(recive_data); //Emit Request is finished && Pass recived data!
}

void qhttpc::sync_request_reply_finished(QNetworkReply *reply)
{
    m_response->set_body(reply->readAll());
    m_response->set_status_code(m_network_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt());
    m_network_reply->deleteLater();
    m_event_loop.exit();
}

QString qhttpc::error_string() const
{
    return m_error_string;
}

bool qhttpc::is_error() const
{
    return m_is_error;
}

int qhttpc::status_code()
{
    return m_status_code;
}
